exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('persons').truncate()
      .then(function () {
        // Inserts seed entries
        return knex('persons').insert([
          {name: "lampt", age: "12", gender: "male", created_at: knex.fn.now(), updated_at: knex.fn.now()},
          {name: "minhlm", age: "23", gender: "female", created_at: knex.fn.now(), updated_at: knex.fn.now()},
          {name: "loanpv", age: "97", gender: "female", created_at: knex.fn.now(), updated_at: knex.fn.now()},
        ]);
      });
  };
  